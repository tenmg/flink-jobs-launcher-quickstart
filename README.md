# flink-jobs-launcher-quickstart

## 介绍
flink-jobs-launcher-quickstart是一个简单的网页应用，用于带领开发者快速入门[flink-jobs-launcher](https://gitee.com/tenmg/flink-jobs-launcher)。该网站只有一个主页面，可以启动和停止一个flink-jobs应用程序或普通flink应用程序。程序被启动后，页面还可以显示一些简单的监控信息。

flink-jobs-launcher-quickstart演示了如何通过[flink-jobs-launcher](https://gitee.com/tenmg/flink-jobs-launcher)将[flink-jobs](https://gitee.com/tenmg/flink-jobs)集成到现有基于Java应用中（例如JavaWeb等）。通常而言，应用系统和flink集群是分离的，且应用系统可能对外网提供服务，而flink集群只在内网运行。所以实际应用当中往往还需要一个转发机制，将应用程序的请求转发到[flink-jobs-launcher](https://gitee.com/tenmg/flink-jobs-launcher)单独的启动程序。但为了方便，本例中将应用系统安装在flink集群的其中一台服务器上，免除了单独开发启动程序和请求转发的工作。
## 安装教程

1.  需先准备flink和tomcat运行环境，tomcat装在其中一台flink服务器上，flink需要配置`state.savepoints.dir`以便可以通过[flink-jobs-launcher](https://gitee.com/tenmg/flink-jobs-launcher)的启动器停止任务。

2.  下载[flink-jobs-launcher-quickstart](https://gitee.com/tenmg/flink-jobs-launcher-quickstart)并根据实际运行环境修改配置文件`main/resources/app.properties`后运行`mvn clean package`打包。

3.  将WAR包上传到tomcat的webapps目录下，并启动tomcat和flink。

4.  浏览器打开主页http://${youraddress}:8080/flink-jobs-launcher-quickstart/，可以看到文本框中默认的flink-jobs配置内容。
![flink-jobs-launcher-quickstart主页](https://images.gitee.com/uploads/images/2021/0824/133902_7cefa761_7920102.png "flink-jobs-launcher-quickstart主页.png")


## 使用说明

1.  打开主页后，可修改配置内容后点击“启动”按钮启动flink-jobs任务，也可以直接点击“启动”按钮启动flink的WordCount样例程序。关于flink-jobs的详细配置详见https://gitee.com/tenmg/flink-jobs-launcher。

![启动flink-jobs任务](https://images.gitee.com/uploads/images/2021/0824/135515_0d9ca890_7920102.png "启动flink-jobs任务.png")

2.  任务启动后，网页便开始计时，并不断请求后台监控任务运行的情况，主页上会显示相关信息。

![监控flink-jobs任务](https://images.gitee.com/uploads/images/2021/0824/135704_94864c1a_7920102.png "监控flink-jobs任务.png")

3.  任务启动后，可以随时关闭任务，关闭任务后会返回保存点，将返回的保存点存储起来，再下次提交任务时作为启动选项提交给[flink-jobs-launcher](https://gitee.com/tenmg/flink-jobs-launcher)可实现从保存点重新恢复或重启任务的效果。

![停止flink-jobs任务](https://images.gitee.com/uploads/images/2021/0824/140938_e8dce7d9_7920102.png "停止flink-jobs任务.png")

## 玩转flink

将flink-jobs任务配置内容保存在数据库中，通过flink-jobs-launcher启动任务后，将返回的`jobsId`保存到数据库的任务运行日志中，再使用`jobsId`通过flink提供的接口监控任务可快速将flink彻底集成到现有系统中，实现统一平台管理。

![玩转flink](https://images.gitee.com/uploads/images/2021/0824/155531_ef090a42_7920102.png "玩转flink.png")

## 相关链接

flink-jobs开源地址：https://gitee.com/tenmg/flink-jobs

flink-jobs-launcher开源地址：https://gitee.com/tenmg/flink-jobs-launcher

DSL开源地址：https://gitee.com/tenmg/dsl

Flink官网：https://flink.apache.org

Debezuim官网：https://debezium.io
