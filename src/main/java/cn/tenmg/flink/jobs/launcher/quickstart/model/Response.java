package cn.tenmg.flink.jobs.launcher.quickstart.model;

/**
 * 请求返回结果
 * 
 * @author June wjzhao@aliyun.com
 *
 */
public class Response {

	private boolean success;

	private String message;

	private Object data;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public static Response success(boolean success) {
		Response response = new Response();
		response.success = success;
		return response;
	}

	public Response message(String message) {
		this.message = message;
		return this;
	}

	public Response data(Object data) {
		this.data = data;
		return this;
	}

}
