package cn.tenmg.flink.jobs.launcher.quickstart.utils;

import java.io.IOException;
import java.util.Properties;

import cn.tenmg.flink.jobs.launcher.utils.PropertiesLoaderUtils;

/**
 * 应用工具类
 * 
 * @author June wjzhao@aliyun.com
 *
 */
public class AppUtils {

	private static String flinkHome;

	private static String flinkRest;

	static {
		try {
			Properties properties = PropertiesLoaderUtils.loadFromClassPath("app.properties");
			flinkHome = properties.getProperty("flink.home");
			flinkRest = properties.getProperty("flink.rest");
		} catch (IOException e) {
			flinkHome = "/opt/flink";
			flinkRest = "http://localhost:8081";
		}
	}

	public static String getFlinkHome() {
		return flinkHome;
	}

	public static String getFlinkRest() {
		return flinkRest;
	}

}
