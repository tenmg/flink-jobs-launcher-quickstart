package cn.tenmg.flink.jobs.launcher.quickstart.exception;

/**
 * 请求处理异常
 * 
 * @author June wjzhao@aliyun.com
 *
 */
public class RequestResolveException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2485639111956832745L;

	public RequestResolveException() {
		super();
	}

	public RequestResolveException(String massage) {
		super(massage);
	}

	public RequestResolveException(Throwable cause) {
		super(cause);
	}

	public RequestResolveException(String massage, Throwable cause) {
		super(massage, cause);
	}
}
